const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    viewportWidth: 1920,
    viewportHeight: 1080,
    baseUrl: "https://ecommerce-playground.lambdatest.io/index.php",
    supportFile: false,
  },
});
