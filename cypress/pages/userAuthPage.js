export class userAuth {
  firstName = "input[id='input-firstname']";
  lastName = "input[id='input-lastname']";
  email = "input[id='input-email']";
  phone = "input[id='input-telephone']";
  password = "input[id='input-password']";
  confimPassword = "input[id='input-confirm']";
  continueButton = "input[value='Continue']";
  agreeButton = "label[for='input-agree']";
  loginButton = "input[value='Login']";

  enterFirstName(firstName) {
    cy.get(this.firstName).type(firstName);
  }

  enterLastName(lastName) {
    cy.get(this.lastName).type(lastName);
  }

  enterEmail(email) {
    cy.get(this.email).type(email);
  }

  enterTelephone(phone) {
    cy.get(this.phone).type(phone);
  }

  enterPassword(password) {
    cy.get(this.password).type(password);
  }

  enterConfirmPassword(password) {
    cy.get(this.confimPassword).type(password);
  }

  clickContinue() {
    cy.get(this.continueButton).click();
  }

  clickAgreement() {
    cy.get(this.agreeButton).click();
  }

  clickLogin() {
    cy.get(this.loginButton).click();
  }

  userEmptyFieldsNotificationsVisibility() {
    cy.contains("First Name must be between 1 and 32 characters!").should(
      "be.visible"
    );
    cy.contains("Last Name must be between 1 and 32 characters!").should(
      "be.visible"
    );
    cy.contains("E-Mail Address does not appear to be valid!").should(
      "be.visible"
    );
    cy.contains("Telephone must be between 3 and 32 characters!").should(
      "be.visible"
    );
    cy.contains("Password must be between 4 and 20 characters!").should(
      "be.visible"
    );
  }

  createAccountSuccesNotifiactionVisibility() {
    cy.contains("Your Account Has Been Created!").should("be.visible");
  }

  emailNotificationVisibility() {
    cy.contains("E-Mail Address does not appear to be valid!").should(
      "be.visible"
    );
  }

  passwordNotificationVisibility() {
    cy.contains("Password confirmation does not match password!").should(
      "be.visible"
    );
  }

  loginSucessfullValidation() {
    cy.contains("Edit your account information").should("be.visible");
  }
}
