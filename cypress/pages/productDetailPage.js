export class productDetail {
  addToCartButton =
    "button[class='text btn btn-md btn-secondary btn-block btn-cart button-cart cart-34']";
  productName = "#entry_216816 h1";
  editCartButton =
    "a[class='icon-right both btn btn-primary btn-lg btn-block']";

  clickAddToCart() {
    cy.get(this.addToCartButton).last().click();
  }

  clickEditCart() {
    cy.get(this.editCartButton).click();
  }
}
