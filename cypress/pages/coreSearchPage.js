export class coreSearch {
  gridContent = "div[data-id='212469']";
  searchInput = "input[placeholder='Search For Products']";
  searchButton = "button[class='type-text']";
  inStockCheckbox = "#mz-filter-panel-0-4 .custom-control-label";
  productsItem = "div[class='carousel-inner']";
  linkToCart = "div[class='cart-icon']";

  searchProduct(productName) {
    cy.get(this.searchInput).first().type(productName);
  }

  clickSearch() {
    cy.get(this.searchButton).first().click();
  }

  clickInStockCheckbox() {
    cy.get(this.inStockCheckbox).click();
    cy.wait(2000);
    // dynamic waits doesn't work properly
  }

  getGridContentVisibility() {
    cy.get(this.gridContent).should("be.visible");
  }

  clickOpenCart() {
    cy.get(this.linkToCart).first().click();
  }

  openProductDetailPage() {
    cy.get(this.productsItem).first().click();
  }

  productsNumberValidation(productsNumber) {
    cy.get(this.productsItem).then(($elements) => {
      expect($elements.length).to.equal(productsNumber);
    });
  }

  noProductNotificationVisibility() {
    cy.contains("There is no product that matches the search criteria.").should(
      "be.visible"
    );
  }
}
