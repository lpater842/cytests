import { coreSearch } from "../../pages/coreSearchPage";

const products = require("../../fixtures/products.json");
const core = new coreSearch();

describe("Product Search and Filter", () => {
  beforeEach(() => {
    // Navigate to the homepage or product search page
    cy.visit("");
  });
  it("should use search and filter functions to not find given products", () => {
    // Use the search function to find products"
    core.searchProduct(products.eletronic);
    core.clickSearch();
    // No elemetns found
    core.noProductNotificationVisibility();
  });

  it("should use search and filter functions to find given products", () => {
    // Use the search function to find products"
    core.searchProduct(products.apple);
    core.clickSearch();
    core.getGridContentVisibility();
    core.clickInStockCheckbox();
    core.productsNumberValidation(8);
  });
});
