import { productDetail } from "../../pages/productDetailPage";
import { coreSearch } from "../../pages/coreSearchPage";
import { cart } from "../../pages/cartPage";
import "../../support/commands";

const products = require("../../fixtures/products.json");

const product = new productDetail();
const core = new coreSearch();
const ca = new cart();
let productName;
let cartProductName;

describe("Adding Items to Cart", () => {
  it("should add a product to the cart and verify the cart contents", () => {
    // Navigate to the homepage or product search page
    cy.visit("");

    // Use the search function to find products"
    core.searchProduct(products.apple);
    core.clickSearch();
    core.openProductDetailPage();

    cy.getTextFromElement("#entry_216816 h1").then((productName) => {
      cy.log("element name:", productName);
    });

    // Adding product to cart
    product.clickAddToCart();

    // Open cart
    core.clickOpenCart();
    product.clickEditCart();

    cy.getTextFromElement(".text-left a").then((cartProductName) => {
      cy.log("element name:", cartProductName);
    });

    // Check product name is in the cart
    expect(productName).to.eq(cartProductName);
  });
});
