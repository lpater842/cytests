import { userAuth } from "../../pages/userAuthPage";

const userData = require("../../fixtures/user.json");
const authFlow = new userAuth();

describe("User auth flow tests", () => {
  // Generate random username, email
  const firstName = "testuser" + Math.floor(Math.random() * 1000000);
  const lastName = "testuser" + Math.floor(Math.random() * 1000000);
  const email = firstName + "@example.com";

  it("should register a new user successfully", () => {
    // Go to register site
    cy.visit("?route=account/register");

    // Fill user data in registration form
    authFlow.enterFirstName(firstName);
    authFlow.enterLastName(lastName);
    authFlow.enterEmail(email);
    authFlow.enterTelephone(userData.telephoneNumber);
    authFlow.enterPassword(userData.password);
    authFlow.enterConfirmPassword(userData.password);
    authFlow.clickAgreement();
    authFlow.clickContinue();

    // Verify successful registration and redirect to the login page
    authFlow.createAccountSuccesNotifiactionVisibility();
  });

  it("should register empty fields validate properly ", () => {
    // Go to register site
    cy.visit("?route=account/register");

    // Click contiune and check validation
    authFlow.clickContinue();
    authFlow.userEmptyFieldsNotificationsVisibility();
  });

  it("should given register fields validate properly ", () => {
    // Go to register site
    cy.visit("?route=account/register");

    // Check example validation
    authFlow.enterPassword(userData.password);
    authFlow.enterConfirmPassword(userData.wrongPassword);
    authFlow.clickContinue();
    authFlow.passwordNotificationVisibility();
    authFlow.enterEmail(userData.wrongEmail);
    authFlow.clickContinue();
    authFlow.emailNotificationVisibility();
  });

  it("should login successfully", () => {
    // Go to login site
    cy.visit("?route=account/login");

    // Click and log in with the newly created credentials
    authFlow.enterEmail(email);
    authFlow.enterPassword(userData.password);
    authFlow.clickLogin();

    // Confirm that the login is successful and the user is directed to the homepage
    authFlow.loginSucessfullValidation();
  });
});
