Cypress.Commands.add("getTextFromElement", (locator) => {
  return cy.get(locator).then(($element) => {
    return $element.text();
  });
});
